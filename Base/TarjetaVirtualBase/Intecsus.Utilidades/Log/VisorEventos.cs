﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Intecsus.Utilidades.Log
{
    public class VisorEventos
    {

        /// <summary>
        /// Metodo encargado de almacenar informacion en el visor de evento
        /// </summary>
        /// <param name="nombreLog">Nombre del log--- Application </param>
        /// <param name="nombreSource">Nombre q le pones al Source --- Error base de datos</param>
        /// <param name="mensaje">Mensaje q se quiere reportar</param>
        /// <param name="tipoEvento">Tipo de evento a almacenar EventLogEntryType.Warning</param>
        /// <remarks>AERM 18/02/2016</remarks>
        public void GuardarLog(string nombreLog, string nombreSource, string mensaje, EventLogEntryType tipoEvento)
        {
            try
            {
                EventLog evento = new EventLog();
                if (!EventLog.SourceExists(nombreSource))
                {
                    EventLog.CreateEventSource(nombreSource, nombreLog);
                }

                evento.Log = nombreLog;
                evento.Source = nombreSource;
                evento.EnableRaisingEvents = true;
                evento.WriteEntry(mensaje, tipoEvento);

            }
            catch (Exception ex)
            {
            }
        }
    }
}
