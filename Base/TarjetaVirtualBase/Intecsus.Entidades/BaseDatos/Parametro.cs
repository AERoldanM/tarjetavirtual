﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Intecsus.Entidades.BaseDatos
{
    using Microsoft.VisualBasic;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Data;
    using System.Diagnostics;
    /// <summary>
    ///  Representa un parametro dentro de la B.D
    /// </summary>
    /// <remarks>AERM 20150119</remarks>
    public class Parametro<T>
    {
        private string m_Nombre;
        private int m_Tamano;
        private T m_TipoDato;

        private string m_ValorEnviar;
        /// <summary>
        /// Nombre del usuario
        /// </summary>
        public string nombre
        {
            get { return m_Nombre; }
            set { m_Nombre = value; }
        }

        /// <summary>
        /// Tamanño maximo dentro de la B.D
        /// </summary>
        public int tamano
        {
            get { return m_Tamano; }
            set { m_Tamano = value; }
        }

        /// <summary>
        /// Representa el tipo de dato a enviar
        /// </summary>
        public T tipoDato
        {
            get { return m_TipoDato; }
            set { m_TipoDato = value; }
        }

        public string valorEnviar
        {
            get { return m_ValorEnviar; }
            set { m_ValorEnviar = value; }
        }
    }
}
