﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Intecsus.Entidades.BaseDatos
{
    /// <summary>
    /// Retorna un parametro segun los valores enviados
    /// </summary>
    public class CreadorParametros
    {
        /// <summary>
        /// Metodo encargado de retornar un parametro SQLSERVER
        /// </summary>
        /// <param name="nombreAEnviar">Nombre a enviar segun creación del procedimiento</param>
        /// <param name="valor">Valor a enviar dentro de la variable</param>
        /// <param name="tipoDato">Tipo de dato q recibe la base de datos</param>
        /// <param name="tamano">Tamaño del dato</param>
        /// <returns>AERM 30-01-2017</returns>
        public Parametro<SqlDbType> ParamentroSqlServer(string nombreAEnviar, string valor, SqlDbType tipoDato, Int32 tamano)
        {
            Parametro<SqlDbType> _parametro = new Parametro<SqlDbType>();
            _parametro.nombre = nombreAEnviar;
            _parametro.valorEnviar = valor;
            _parametro.tipoDato = tipoDato;
            _parametro.tamano = tamano;

            return _parametro;
        }

        /// <summary>
        /// Metodo encargado de retornar un parametro SQLSERVER
        /// </summary>
        /// <param name="nombreAEnviar">Nombre a enviar segun creación del procedimiento</param>
        /// <param name="valor">Valor a enviar dentro de la variable</param>
        /// <param name="tipoDato">Tipo de dato q recibe la base de datos</param>
        /// <param name="tamano">Tamaño del dato</param>
        /// <returns>AERM 30-01-2017</returns>
        public Parametro<MySqlDbType> ParamentroMYSQL(string nombreAEnviar, string valor, MySqlDbType tipoDato, Int32 tamano)
        {
            Parametro<MySqlDbType> _parametro = new Parametro<MySqlDbType>();
            _parametro.nombre = nombreAEnviar;
            _parametro.valorEnviar = valor;
            _parametro.tipoDato = tipoDato;
            _parametro.tamano = tamano;

            return _parametro;
        }
    }
}
