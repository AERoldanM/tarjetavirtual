﻿using Intecsus.Entidades.BaseDatos;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Intecsus.Conexion
{
    /// <summary>
    /// Interface que expone los metodos a ejecutar
    /// </summary>
    /// <remarks>AERM 20150119</remarks>
    public interface IDALBaseDatos
    {

        /// <summary>
        /// Metodo encargado de implementar la ejecucion de un store procedure
        /// </summary>
        /// <param name="conexionBD">Linea de conexion con la base de datos</param>
        /// <param name="nombreSP">Nombre del strore procedure a ejecutar</param>
        /// <param name="nombreTablaReturn">Nombre de la tabla que retorna</param>
        /// <param name="parametros">Lista de parametros a ejecutar</param>
        /// <returns>Retorna una tabal de respuesta</returns>
        /// <remarks>AERM 20150119</remarks>
        DataSet EjecutarSP(string conexionBD, string nombreSP, string nombreTablaReturn, List<Parametro<SqlDbType>> parametros);

        /// <summary>
        /// Metodo encargado de implementar la ejecucion de un store procedure sin definir en los parametros el tamaño del valor
        /// </summary>
        /// <param name="conexionBD">Linea de conexion con la base de datos</param>
        /// <param name="nombreSP">Nombre del strore procedure a ejecutar</param>
        /// <param name="nombreTablaReturn">Nombre de la tabla que retorna</param>
        /// <param name="parametros">Lista de parametros a ejecutar</param>
        /// <returns>Retorna una tabal de respuesta</returns>
        /// <remarks>AERM 20150119</remarks>
        DataSet EjecutarSPSinTamano(string conexionBD, string nombreSP, string nombreTablaReturn, List<Parametro<SqlDbType>> parametros);

        /// <summary>
        /// Ejecuta Bulk Insert
        /// </summary>
        /// <param name="DTOrigen">Datatable: DataTable donde se tiene la informacion</param>
        /// <param name="TablaDestino">String: Nombre de la tabal de destino donde va insertar la informacion</param>
        /// <param name="connectionString">String: cadena de conexion</param>
        /// <returns>String: con informacion de carga</returns>
        ///<remarks>AERM 20150119</remarks>
        string SubirDataTable(DataTable dtOrigen, string tablaDestino, string connectionString);

        /// <summary>
        /// Ejecuta una sentecia SQL no aconsejado
        /// </summary>
        ///  <param name="conexionBD">Linea de conexion con la base de datos</param>
        ///  <param name="nombreTablaReturn">Nombre de la tabla que retorna</param>
        /// <param name="sentencia">Sentencia a ejecutar en la base de datos</param>
        /// <param name="tablaDestino">Nombre de la tabla como retornan los datos</param>
        /// <returns>Retorna una tabal de respuesta</returns>
        /// <remarks>AERM 20150119</remarks>
        DataSet EjecutarSentencia(string conexionBD, string nombreTablaReturn, string sentencia, string tablaDestino);

    }
}
