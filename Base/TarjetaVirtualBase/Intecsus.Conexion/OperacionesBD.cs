﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using Intecsus.Conexion.Implementaciones;

namespace Intecsus.Conexion
{
    public class OperacionesBD
    {
        /// <summary>
        /// Crea el objeto de la instancia
        /// </summary>
        /// <remarks>AERM 20/01/2015</remarks>
        private static OperacionesBD _instancia;

        private IDALBaseDatos conexion = null;
       
        /// <summary>
        /// No causar problemas si es multihilo (Si tiene varias peticiones a  la vez)
        /// </summary>
        /// <remarks>AERM 20/01/2015</remarks>
        private static object padlock = new object();
        /// <summary>
        /// Método que instancia solo una vez la clase de conexion atravez de una fachada
        /// </summary>
        /// <remarks>AERM 20/01/2015</remarks>
        private OperacionesBD()
        {
            //Aqui podrian ir un facade con la Conexión
            conexion = this.operarBD(ConfigurationManager.AppSettings["BD"].ToString());
        }

        /// <summary>
        /// Método que realiza la instancia mediante una propiedad
        /// </summary>
        public static OperacionesBD Instancia
        {
            get
            {
                //Hasta que no termine la petición no recibe la otra (Caso multihilo)
                lock (padlock)
                {
                    // valida que no exista una instancia si ya existe, no la realiza
                    if ((_instancia == null))
                    {
                        _instancia = new OperacionesBD();
                    }
                }

                return _instancia;
            }
        }

        /// <summary>
        /// Devuelve la instacia ya creada con el singleton
        /// </summary>
        /// <returns></returns>
        /// <remarks>AERM 20/01/2015</remarks>
        public IDALBaseDatos operarBD()
        {
            return conexion;
        }

        /// <summary>
        /// Crea la instacion segun el tipo de bd
        /// </summary>
        /// <param name="baseDatos"></param>
        /// <returns></returns>
        /// <remarks>AERM 20/01/2015</remarks>
        private IDALBaseDatos operarBD(string baseDatos)
        {
            try
            {
                return this.TipoBaseDeDatosConsulta(baseDatos);
            }
            catch (Exception ex)
            {
                string mensaje = ex.Message;
                throw;
            }
        }

        /// <summary>
        /// Consulta el tipo de base de datos al cual se esta conectando segun la fabrica creada por el proveedor
        /// </summary>
        /// <param name="db">Bas e de datos de la conexión</param>
        /// <returns>Tipo de Base de datos</returns>
        /// <remarks>AERM 20/01/2015</remarks>
        private IDALBaseDatos TipoBaseDeDatosConsulta(string db)
        {
            try
            {
                switch (db)
                {
                    case "SqlServer":
                        return new DALBaseDatosSQL();
                    default:
                        throw new Exception("No está disponible la base de datos. (OperacionesBD - Datos)");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
