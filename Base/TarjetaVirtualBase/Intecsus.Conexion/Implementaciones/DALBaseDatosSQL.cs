﻿using Intecsus.Entidades.BaseDatos;
using Intecsus.Utilidades.Log;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Intecsus.Conexion.Implementaciones
{
    public class DALBaseDatosSQL : IDALBaseDatos
    {

        #region "Instanciar Clases"
        #endregion
        VisorEventos visorEventos = new VisorEventos();

        #region "Implementacion Interface"

        public DataSet EjecutarSentencia(string conexionBD, string nombreTablaReturn, string sentencia, string tablaDestino)
        {
            DataSet topics = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            try
            {
                SqlCommand objCommand = new SqlCommand();
                objCommand.CommandType = CommandType.Text;
                objCommand.CommandText = sentencia;
                objCommand.Connection = new SqlConnection(conexionBD);
                try
                {
                    objCommand.Connection.Open();
                    adapter = new SqlDataAdapter(objCommand);
                    adapter.Fill(topics, nombreTablaReturn);
                    objCommand.Connection.Close();
                    objCommand.Parameters.Clear();
                }
                finally
                {
                    if (adapter != null)
                    {
                        adapter.Dispose();
                    }
                }
                return topics;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                topics.Dispose();
            }
        }

        public DataSet EjecutarSP(string conexionBD, string nombreSP, string nombreTablaReturn, List<Parametro<SqlDbType>> parametros)
        {
            DataSet topics = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            try
            {
                SqlCommand objCommand = this.ObtenerParametros(nombreSP, parametros);

                objCommand.Connection = new SqlConnection(conexionBD);
                objCommand.CommandTimeout = 60;
                objCommand.Connection.Open();
                try
                {
                    adapter = new SqlDataAdapter(objCommand);
                    adapter.Fill(topics, nombreTablaReturn);
                    objCommand.Connection.Close();
                    objCommand.Parameters.Clear();
                }
                finally
                {
                    if (adapter != null)
                    {
                        adapter.Dispose();
                    }
                }
                return topics;
            }
            catch (Exception ex)
            {
                visorEventos.GuardarLog("Application", "Intecsus.BD", "EjecutarSP " + ex.Message, EventLogEntryType.Error);
                return topics;
            }
            finally
            {
                topics.Dispose();
            }
        }

        public DataSet EjecutarSPSinTamano(string conexionBD, string nombreSP, string nombreTablaReturn, List<Parametro<SqlDbType>> parametros)
        {
            DataSet topics = new DataSet();
            SqlDataAdapter adapter = new SqlDataAdapter();
            try
            {
               SqlCommand objCommand = this.ObtenerParametrosSinTamano(nombreSP, parametros);

                objCommand.Connection = new System.Data.SqlClient.SqlConnection(conexionBD);
                objCommand.Connection.Open();
                try
                {
                    adapter = new System.Data.SqlClient.SqlDataAdapter(objCommand);
                    adapter.Fill(topics, nombreTablaReturn);
                    objCommand.Connection.Close();
                    objCommand.Parameters.Clear();
                }
                finally
                {
                    if (adapter != null)
                    {
                        adapter.Dispose();
                    }
                }
                return topics;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                topics.Dispose();
            }
        }

        public string SubirDataTable(DataTable dtOrigen, string tablaDestino, string connectionString)
        {
            try
            {
                using (SqlBulkCopy bulkCopy = new SqlBulkCopy(connectionString, System.Data.SqlClient.SqlBulkCopyOptions.KeepNulls))
                {
                    bulkCopy.DestinationTableName = tablaDestino;
                    bulkCopy.BulkCopyTimeout = 0;
                    try
                    {
                        bulkCopy.WriteToServer(dtOrigen);
                        return "OK";
                    }
                    catch (Exception ex)
                    {
                        return "NOK:" + ex.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                return "NOK:" + ex.ToString();
            }
        }
        #endregion

        #region "Metodos Privados"

        /// <summary>
        /// Metodo encargado de enviar los parametros
        /// </summary>
        /// <param name="nombreSP">Nombre del storeProcedure</param>
        /// <param name="parametros">Parametros a enviar</param>
        /// <returns>retorna objeto con el comando a ejecutar</returns>
        /// <remarks>AERM 20150119</remarks>
        private SqlCommand ObtenerParametros(string nombreSP, List<Parametro<SqlDbType>> parametros)
        {

            try
            {
               SqlCommand objCommand = new SqlCommand(nombreSP);
                objCommand.CommandType = CommandType.StoredProcedure;
                foreach (var parametro in parametros)
                {
                    objCommand.Parameters.Add(parametro.nombre, parametro.tipoDato, parametro.tamano);
                    objCommand.Parameters[parametro.nombre].Value = parametro.valorEnviar;
                }

                return objCommand;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Metodo encargado de enviar los parametros
        /// </summary>
        /// <param name="nombreSP">Nombre del storeProcedure</param>
        /// <param name="parametros">Parametros a enviar</param>
        /// <returns>retorna objeto con el comando a ejecutar</returns>
        /// <remarks>AERM 20150119</remarks>
        private System.Data.SqlClient.SqlCommand ObtenerParametrosSinTamano(string nombreSP, List<Parametro<SqlDbType>> parametros)
        {

            try
            {
                System.Data.SqlClient.SqlCommand objCommand = new System.Data.SqlClient.SqlCommand(nombreSP);
                objCommand.CommandType = CommandType.StoredProcedure;
                foreach (var parametro in parametros)
                {
                    objCommand.Parameters.Add(parametro.nombre, parametro.tipoDato, parametro.tamano);
                    objCommand.Parameters[parametro.nombre].Value = parametro.valorEnviar;
                }

                return objCommand;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }

}
